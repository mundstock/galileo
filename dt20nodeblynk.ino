#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include <DHT.h>
ESP8266WebServer server(80);
#define LEDPIN LED_BUILTIN           // For LED Blinking
#define DHTPIN D1          // For DHT Sensor
 
// Uncomment whatever type you're using!
#define DHTTYPE DHT11     // DHT 11
//#define DHTTYPE DHT22   // DHT 22, AM2302, AM2321
//#define DHTTYPE DHT21   // DHT 21, AM2301
 int pinValue = 0;
DHT dht(DHTPIN, DHTTYPE);
const char* ssid = "poa.hub";//your ssid
const char* password =  "coworking";//your password
float h;
float t;
unsigned long previousMillis = 0;
unsigned long interval = 1000;
void setup() {
  pinMode(LEDPIN, OUTPUT);
    Serial.begin(115200);
     dht.begin();
 
 
 
    WiFi.begin(ssid, password);  //Connect to the WiFi network
 
    while (WiFi.status() != WL_CONNECTED) {  //Wait for connection
 
        delay(500);
        Serial.println("Waiting to connect...");
 
    }
 
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());  //Print the local IP
 
   
   server.on("/GetData", handleGet); //Associate the handler function to the path
    server.begin(); //Start the server
    Serial.println("Server listening");
 
}
 
void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
   sendSensor();
  }
server.handleClient(); //Handling of incoming requests
}
 
void sendSensor()
{
   h = dht.readHumidity();
   t = dht.readTemperature(); // or dht.readTemperature(true) for Fahrenheit
 
  if (isnan(h) || isnan(t)) { 
  Serial.println("Failed to read from DHT sensor!");
  digitalWrite(LEDPIN, LOW);
  delay(30);
  digitalWrite(LEDPIN, HIGH);
  delay(100);
  digitalWrite(LEDPIN, LOW);
  delay(30);
  digitalWrite(LEDPIN, HIGH);
  delay(1000);
    return;
  }
  else {
  digitalWrite(LEDPIN, LOW);
  delay(30);
  digitalWrite(LEDPIN, HIGH);
  delay(1200);
    }

}
 

void handleGet(){
  String json;
  StaticJsonDocument<200> doc;
  String temp=String(t);
  String humid=String(h);
  doc["temp"] = temp;// reaplce with variable t if dht sensor is available
  doc["humidity"]=humid;// reaplce with variable h if dht sensor is available
serializeJson(doc,json);
server.send(200,"text/json",json);
  }
void handleSend() { 
 
      if (server.hasArg("value")== false){ //Check if body received

            server.send(200, "text/plain", "Body not received");
            return;
 
      }
 
      String message = "value received:\n";
      message += server.arg("value");
        message += "\n";
 
      server.send(200, "text/plain", message);
      Serial.println("Message Received :"+message);
}
